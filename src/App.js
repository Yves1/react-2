import React, {Component} from 'react';
import './App.css';
import {Header} from './components/Header/Header';
import {Home} from './components/Home/Home';
import { Route, Switch} from 'react-router-dom';
import {AddNewContact} from './components/AddNewContact/AddNewContact';
import {Footer }from './components/Footer/Footer';
import {EditContact} from './components/EditContact/EditContact';
import {ContactDetails} from './components/ContactDetails/ContactDetails';



class App extends Component {
  render() {
    return (
      <div>
          <Header/>
          <div className="container">
            <Switch>
              <Route exact path="/AddNewContact" component={AddNewContact}/>
              <Route exact path="/" component ={Home}/>
              <Route exact path="/EditContact/:id" component={EditContact}/>
              <Route exact path="/ContactDetails/:id" component={ContactDetails}/>
            </Switch>
          </div>
          <Footer/>
  
      </div>
    );
   }
  }


export default App;
